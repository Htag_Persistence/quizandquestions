/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epita.quizz.services;

import fr.epita.quizz.datamodels.GenericModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public abstract class GenericDAO {

    protected String TABLE_NAME;
    protected String TABLE_ID;

    public ResultSet selectAll() {
        try {
            String select_query = "SELECT * FROM " + this.TABLE_NAME + " ";
            ConnectionManager cm = ConnectionManager.getInstance();
            PreparedStatement ps = cm.getConnection().prepareStatement(select_query);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }


    public List<GenericModel> formatter(ResultSet resultSet) {
        List<GenericModel> resultList = new ArrayList();
        try {
            while (resultSet.next()) {
//              String name = results.getString("NAME");
//		String address = results.getString("ADDRESS");
//		Customer currentCustomer = new Customer(name, address);
//		resultList.add(currentCustomer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultList;
    }
}
