/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epita.quizz.services;

import fr.epita.quizz.datamodels.Answer;
import fr.epita.quizz.datamodels.MCQChoice;
import fr.epita.quizz.datamodels.QuestionModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class QuestionDAO extends GenericDAO {

    /**
     * Ceci pour differencier les type de questions dans la base de données vu
     * que c'est la meme table qui doit gérer les deux types de questions 1 -->
     * question simple 2 --> question à choix multiple
     */
    private String TABLE_DIFFICULTY;

    public QuestionDAO() {
        this.TABLE_ID = "question";
        this.TABLE_DIFFICULTY = "difficulty";
        this.TABLE_NAME = "QUESTIONS";
    }

    public List<QuestionModel> getAll() {
        return this.format(this.selectAll());
    }

    public List<QuestionModel> getAll(int level) {
        return this.format(this.select10ByDifficulty(level));
    }

    public List<QuestionModel> get10ByDifficulty(int level) {
        return this.format(this.select10ByDifficulty(level));
    }
    
    public List<QuestionModel> get10RandomByTopic(String topic) {
        return this.format(this.select10RandomByTopic(topic));
    }

    public List<QuestionModel> get10Random() {
        return this.format(this.select10Random());
    }

    public ResultSet select10ByDifficulty(int level) {
        try {
            String select_query = "SELECT * FROM " + this.TABLE_NAME + " WHERE " + this.TABLE_DIFFICULTY + "=? LIMIT 10";
            ConnectionManager cm = ConnectionManager.getInstance();
            PreparedStatement ps = cm.getConnection().prepareStatement(select_query);
            ps.setInt(1, level);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public ResultSet select10RandomByTopic(String topic) {
        try {
            String select_query = "SELECT * FROM " + this.TABLE_NAME + " WHERE topics LIKE ? LIMIT 10";
            ConnectionManager cm = ConnectionManager.getInstance();
            PreparedStatement ps = cm.getConnection().prepareStatement(select_query);
            ps.setString(1, "%" + topic + "%");
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet select10Random() {
        try {
            String select_query = "SELECT * FROM " + this.TABLE_NAME + " ORDER BY RAND() LIMIT 10";
            ConnectionManager cm = ConnectionManager.getInstance();
            PreparedStatement ps = cm.getConnection().prepareStatement(select_query);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public String[] getTopics() {
        String topics = "";
        try {
            String select_query = "SELECT topics FROM " + this.TABLE_NAME + " ";
            ConnectionManager cm = ConnectionManager.getInstance();
            PreparedStatement ps = cm.getConnection().prepareStatement(select_query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                topics += rs.getString("topics");
            }
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return topics.split(";");
    }

    public List<QuestionModel> format(ResultSet resultSet) {
        List<QuestionModel> questions = new ArrayList();
        try {
            while (resultSet.next()) {
                //QuestionModel question = new QuestionModel(resultSet.getLong("id"), resultSet.getString("question"), resultSet.getString("topics"), resultSet.getInt("difficulty"));
                QuestionModel question = new QuestionModel(resultSet.getLong("id"), resultSet.getString("question"), resultSet.getString("topics"), resultSet.getInt("difficulty"));
                question.setType(resultSet.getInt("type"));
                question.setChoices(getMCQChoices(question.getId()));
                question.setAnswers(getSimpleAnswer(question.getId()));
                questions.add(question);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return questions;
    }

    public List<Answer> getSimpleAnswer(Long id) {
        List<Answer> answers = new ArrayList();
        try {
            String select_query = "SELECT * FROM answers WHERE question= ? ";
            ConnectionManager cm = ConnectionManager.getInstance();
            PreparedStatement ps = cm.getConnection().prepareStatement(select_query);
            ps.setInt(1, Integer.valueOf(id.toString()));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Answer answer = new Answer(rs.getLong("id"), rs.getLong("question"), rs.getString("text"));
                answer.setJuste(rs.getBoolean("juste"));
                answers.add(answer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return answers;
    }

    public List<MCQChoice> getMCQChoices(Long id) {
        List<MCQChoice> choices = new ArrayList();
        try {
            String select_query = "SELECT * FROM mcqchoices WHERE question= ? ";
            ConnectionManager cm = ConnectionManager.getInstance();
            PreparedStatement ps = cm.getConnection().prepareStatement(select_query);
            ps.setInt(1, Integer.valueOf(id.toString()));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                MCQChoice choice = new MCQChoice(rs.getLong("id"), rs.getString("choice"), rs.getBoolean("valid"));
                choices.add(choice);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return choices;
    }
}
