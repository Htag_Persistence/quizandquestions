/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epita.quizz.datamodels;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Persistence
 */
public class QuestionModel {

    private String question;
    private Long id;
    private String selected;
    private List<String> selections;
    private List<String> topics;
    private Integer difficulty;
    private Integer type;
    List<MCQChoice> choices;
    private List<MCQAnswer> mcqAnswers;
    private List<Answer> answers;

    public QuestionModel(String question, Long id, List<String> topics, Integer difficulty, Integer type) {
        this.question = question;
        this.id = id;
        this.topics = topics;
        this.difficulty = difficulty;
        this.type = type;
    }

    public QuestionModel(Long id, String question, String topics, Integer difficulty) {
        this.question = question;
        this.topics = new ArrayList();
        answers = new ArrayList();
        this.id = id;
        for (String topic : topics.split(";")) {
            this.topics.add(topic);
        }
        this.difficulty = difficulty;
    }

    public QuestionModel(Long id, List<String> topics, Integer difficulty, Integer type, List<MCQChoice> choices, List<MCQAnswer> mcqAnswers) {
        this.id = id;
        this.topics = topics;
        this.difficulty = difficulty;
        this.type = type;
        this.choices = choices;
        this.mcqAnswers = mcqAnswers;
    }

    public QuestionModel(Long id, String question, List<String> topics, Integer difficulty, Integer type, List<MCQChoice> choices, List<MCQAnswer> mcqAnswers) {
        this.id = id;
        this.topics = topics;
        this.difficulty = difficulty;
        this.type = type;
        this.question = question;
        this.choices = choices;
        this.mcqAnswers = mcqAnswers;
    }

    public QuestionModel(String question, String topics, Integer difficulty) {
        this.question = question;
        for (String topic : topics.split(";")) {
            this.topics.add(topic);
        }
        this.difficulty = difficulty;
        this.topics = new ArrayList();
        this.choices = new ArrayList();
        this.mcqAnswers = new ArrayList();
    }

    public QuestionModel(Question q, List<MCQChoice> choices, List<MCQAnswer> answers) {
        this.question = q.getQuestion();
        this.topics = q.getTopics();
        this.difficulty = q.getDifficulty();
        this.choices = choices;
        this.mcqAnswers = answers;
    }

    public MCQChoice getCorrectChoice() {
        MCQChoice choix = new MCQChoice();
        for (MCQChoice choice : this.choices) {
            if (choice.isValid()) {
                choix = choice;
            }
        }
        return choix;
    }

    public Answer getCorrectAnswer() {
        Answer answ = new Answer();
        for (Answer ans : this.answers) {
            if (ans.isJuste()) {
                answ = ans;
            }
        }
        return answ;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }

    public Integer getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Integer difficulty) {
        this.difficulty = difficulty;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public List<MCQChoice> getChoices() {
        return choices;
    }

    public void setChoices(List<MCQChoice> choices) {
        this.choices = choices;
    }

    public List<MCQAnswer> getMcqAnswers() {
        return mcqAnswers;
    }

    public void setMcqAnswers(List<MCQAnswer> mcqAnswers) {
        this.mcqAnswers = mcqAnswers;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public List<String> getSelections() {
        return selections;
    }

    public void setSelections(List<String> selections) {
        this.selections = selections;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QuestionModel other = (QuestionModel) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "QuestionModel{" + "question=" + question + ", id=" + id + ", topics=" + topics + ", difficulty=" + difficulty + ", type=" + type + ", choices=" + choices + ", mcqAnswers=" + mcqAnswers + ", answers=" + answers + '}';
    }

}
