/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epita.quizz.datamodels;

import java.util.Objects;

/**
 *
 * @author dell
 */
public class MCQChoice {

    private Long id;
    private String choice;
    private boolean valid;
    private Question question;

    public MCQChoice() {
        question = new Question();
    }

    public MCQChoice(Long id, String choice, boolean valid) {
        this.id = id;
        this.choice = choice;
        this.valid = valid;
    }

    public MCQChoice(String choice, boolean valid) {
        this.choice = choice;
        this.valid = valid;
        question = new Question();
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.choice);
        hash = 29 * hash + (this.valid ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MCQChoice other = (MCQChoice) obj;
        if (this.valid != other.valid) {
            return false;
        }
        if (!Objects.equals(this.choice, other.choice)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MCQChoice{" + "choice=" + choice + ", valid=" + valid + '}';
    }

}
