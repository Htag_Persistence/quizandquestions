/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epita.quizz.datamodels;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author dell
 */
public class MCQQuestion extends Question {

    List<MCQChoice> choices;
    private List<MCQAnswer> mcqAnswers;

    public MCQQuestion(String question, List<String> topics, Integer difficulty) {
        super(question, topics, difficulty);
        this.choices = new ArrayList();
        this.mcqAnswers = new ArrayList();
    }

    public MCQQuestion(String question, String topics, Integer difficulty) {
        super(question, topics, difficulty);
        this.choices = new ArrayList();
        this.mcqAnswers = new ArrayList();
    }

    public MCQQuestion(Question q, List<MCQChoice> choices) {
        this.question = q.getQuestion();
        this.topics = q.getTopics();
        this.difficulty = q.getDifficulty();
        this.choices = new ArrayList();
        this.mcqAnswers = new ArrayList();
    }

    public List<MCQChoice> getChoices() {
        return choices;
    }

    public void setChoices(List<MCQChoice> choices) {
        this.choices = choices;
    }

    public List<MCQAnswer> getMcqAnswers() {
        return mcqAnswers;
    }

    public void setMcqAnswers(List<MCQAnswer> answers) {
        this.mcqAnswers = answers;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MCQQuestion other = (MCQQuestion) obj;
        if (!Objects.equals(this.question, other.question) && !Objects.equals(this.choices, other.choices)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MCQQuestion{" + "choices=" + choices + '}';
    }

}
