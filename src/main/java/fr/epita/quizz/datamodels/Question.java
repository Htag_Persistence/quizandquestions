/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epita.quizz.datamodels;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dell
 */
public class Question extends GenericModel {

    protected String question;
    protected Long id;
    protected List<String> topics;
    protected Integer difficulty;
    protected Integer type;
    private List<Answer> answers;

    public static final int DIFFICULTY_EASY = 1;
    public static final int DIFFICULTY_NORMAL = 2;
    public static final int DIFFICULTY_DIFFICULT = 3;

    public static final int TYPE_SIMPLE = 1;
    public static final int TYPE_MCQ = 2;
    public static final int TYPE_SIMPLE_WITH_IMAGE = 3;
    public static final int TYPE_MCQ_WITH_IMAGE = 4;

    public Question() {
        answers = new ArrayList();
    }

    public Question(String question, List<String> topics, Integer difficulty) {
        this.question = question;
        this.topics = topics;
        this.difficulty = difficulty;
        answers = new ArrayList();
    }

    public Question(String question, String topics, Integer difficulty) {
        this.question = question;
        this.topics = new ArrayList();
        answers = new ArrayList();
        for (String topic : topics.split(";")) {
            this.topics.add(topic);
        }
        this.difficulty = difficulty;
    }

    public Question(Long id, String question, String topics, Integer difficulty) {
        this.question = question;
        this.topics = new ArrayList();
        answers = new ArrayList();
        this.id = id;
        for (String topic : topics.split(";")) {
            this.topics.add(topic);
        }
        this.difficulty = difficulty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getTopics() {
        return topics;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }

    public Integer getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Integer difficulty) {
        this.difficulty = difficulty;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Question{" + "question=" + question + ", id=" + id + ", topics=" + topics + ", difficulty=" + difficulty + ", answers=" + answers + '}';
    }

}
