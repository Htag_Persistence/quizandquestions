/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epita.quizz.beans;

import fr.epita.quizz.datamodels.QuestionModel;
import fr.epita.quizz.services.QuestionDAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Persistence
 */
@ManagedBean(name = "quizBean")
@ViewScoped
public class QuizBean implements Serializable {

    String user;
    int type;
    int number = 0;
    int score = 0;
    int max = 0;
    private List<QuestionModel> questions;
    private final QuestionDAO qdao;

    public QuizBean() {
        this.questions = new ArrayList();
        this.qdao = new QuestionDAO();
        //this.init();
    }

    @PostConstruct
    public void init() {
        if (FacesContext.getCurrentInstance().isPostback()) {
            this.type = Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("type"));
            this.user = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("user");
            String topic = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("topic");
            //System.out.println("This is the template " + this.type + "    dvfgnh  " + this.user);
            if (this.type == 0) {
                this.max = 10;
                if (topic == null) {
                    this.questions = this.qdao.get10Random();
                } else {
this.questions = this.qdao.get10RandomByTopic(topic);
                }
            } else {
                this.questions = this.qdao.get10ByDifficulty(this.type);
                this.max = this.questions.size();
            }
        }
    }

    /**
     * Ici on valide les reponses aux questions puis on calcule la note
     */
    public void goToNext() {
        QuestionModel q = this.questions.get(number);
        switch (q.getType()) {
            case 1:
                if (q.getCorrectAnswer().getId().toString().equals(q.getSelected())) {
                    this.score++;
                }
                break;
            case 2:
                if (q.getSelections().contains(q.getCorrectChoice().getId().toString())) {
                    this.score++;
                }
                break;
            default:
                break;
        }
        if (this.number == (this.max - 1)) {
            RequestContext.getCurrentInstance().execute("PF('dlg2').show()");
        } else {
            this.number++;
        }
        System.out.println(this.score);
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<QuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionModel> questions) {
        this.questions = questions;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

}
