/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epita.quizz.beans;

import fr.epita.quizz.datamodels.Question;
import fr.epita.quizz.datamodels.QuestionModel;
import fr.epita.quizz.services.QuestionDAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.chart.PieChartModel;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author dell
 */
@ManagedBean(name = "questionBean")
@ViewScoped
public class QuestionBean implements Serializable {

    private MenuModel model;

    private List<QuestionModel> questions;
    private List<String> topics;
    private String topic;
    private Question question;
    private QuestionDAO qdao;
    private String user;
    private String query = "";
    private boolean loading = false;
    private boolean skip;

    private PieChartModel pieModel;

    /**
     * Creates a new instance of QuestionBean
     */
    public QuestionBean() {
        this.topic = null;
    }

    @PostConstruct
    public void init() {
        try {
            model = new DefaultMenuModel();
            createPieModel();
            //First submenu
            DefaultSubMenu firstSubmenu = new DefaultSubMenu("Topics");

            this.questions = new ArrayList();
            this.topics = new ArrayList();
            this.qdao = new QuestionDAO();
            this.user = "";
            this.questions = this.qdao.getAll();
            String[] tpcs = this.qdao.getTopics();
            for (String topic : tpcs) {
                if (!this.topics.contains(topic)) {
                    this.topics.add(topic);
                    DefaultMenuItem itm = new DefaultMenuItem(topic);
                    //itm.setIcon("pi pi-home");
                    itm.setUrl("http://www.primefaces.org");
                    firstSubmenu.addElement(itm);
                }
            }
            DefaultMenuItem item = new DefaultMenuItem("About...");
            item.setUrl("http://www.primefaces.org");
            item.setIcon("fa fa-info-circle");
            firstSubmenu.addElement(item);
            model.addElement(firstSubmenu);
            System.out.println("Lancement...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String beginRandom() {
        return "question.xhtml?faces-redirect=true&name=" + this.user;
    }

    public String beginEasy() {
        return "question.xhtml?faces-redirect=true&name=" + this.user + "&type=" + Question.DIFFICULTY_EASY;
    }

    public String beginNormal() {
        return "question.xhtml?faces-redirect=true&name=" + this.user + "&type=" + Question.DIFFICULTY_NORMAL;
    }

    public String beginDifficult() {
        return "question.xhtml?faces-redirect=true&name=" + this.user + "&type=" + Question.DIFFICULTY_DIFFICULT;
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    private void createPieModel() {
        pieModel = new PieChartModel();

        pieModel.set("Brand 1", 540);
        pieModel.set("Brand 2", 325);
        pieModel.set("Brand 3", 702);
        pieModel.set("Brand 4", 421);

        pieModel.setTitle("Custom Pie");
        pieModel.setLegendPosition("e");
        pieModel.setFill(false);
        pieModel.setShowDataLabels(true);
        pieModel.setDiameter(150);
        pieModel.setShadow(false);
    }

    public void updateName() {
        System.out.println("Printed..........................");
        //PrimeFaces.current().executeScript("PF('dlg_uname').hide()");
        //PrimeFaces.current().executeScript("PF('dlg1').show()");
    }

    public void treatement() throws InterruptedException {
        Thread.sleep(1500);
        if (this.user.equals("")) {
            this.user = "";
            //PrimeFaces.current().executeScript("PF('dlg_uname').show()");
        } else {
            //PrimeFaces.current().executeScript("PF('dlg1').show()");
        }
    }

    public List<String> completeText(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            results.add(query + i);
        }
        return results;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public List<QuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionModel> questions) {
        this.questions = questions;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public PieChartModel getPieModel() {
        return pieModel;
    }

    public void setPieModel(PieChartModel pieModel) {
        this.pieModel = pieModel;
    }

}
