-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 14 fév. 2019 à 19:38
-- Version du serveur :  10.1.32-MariaDB
-- Version de PHP :  5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `quiz`
--

-- --------------------------------------------------------

--
-- Structure de la table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `text` varchar(500) NOT NULL,
  `question` int(11) NOT NULL,
  `juste` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `answers`
--

INSERT INTO `answers` (`id`, `text`, `question`, `juste`) VALUES
(1, 'yes', 2, 0),
(2, 'no', 2, 1),
(3, 'all', 1, 0),
(4, 'nothing', 1, 1),
(5, 'maybe', 2, 1),
(6, 'True', 20, 0),
(7, 'False', 20, 1),
(8, 'True', 19, 1),
(9, 'False', 19, 0),
(10, 'True', 18, 1),
(11, 'False', 18, 0),
(12, 'True', 15, 1),
(13, 'False', 15, 0);

-- --------------------------------------------------------

--
-- Structure de la table `mcqchoices`
--

CREATE TABLE `mcqchoices` (
  `id` int(11) NOT NULL,
  `choice` varchar(255) NOT NULL,
  `valid` tinyint(4) NOT NULL,
  `question` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `mcqchoices`
--

INSERT INTO `mcqchoices` (`id`, `choice`, `valid`, `question`) VALUES
(1, 'permeable', 1, 5),
(2, 'reason', 0, 5),
(3, 'studies', 1, 1),
(4, 'marks', 0, 1),
(5, 'proflifate', 1, 3),
(6, 'unprofligate', 0, 3),
(7, 'turpitude', 1, 4),
(8, 'unturpitude', 0, 4),
(9, 'impecunious', 1, 23),
(10, 'impecable', 0, 23);

-- --------------------------------------------------------

--
-- Structure de la table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` varchar(555) DEFAULT NULL,
  `topics` varchar(300) DEFAULT NULL,
  `difficulty` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `questions`
--

INSERT INTO `questions` (`id`, `question`, `topics`, `difficulty`, `type`) VALUES
(1, 'What motivates you?', 'interviews;person;interest;', 1, 2),
(2, 'Do you have any questions?', 'worries;person;self;', 1, 1),
(3, 'Which is a synonym of profligate?', 'synonyms;vocab;word;', 3, 2),
(4, 'Which is a synonym of turpitude?', 'synonyms;vocab;word;', 3, 2),
(5, 'Which is a synonym of bibulous?', 'synonyms;vocab;word;', 3, 2),
(15, 'The Orlando attack happened on Independence Day, the national holiday of the United States.', 'world;history;contry;holidays;', 2, 1),
(18, 'The last VCR was produced on July 22, 2016.', 'virtual;optical;history;', 2, 1),
(19, 'Alfred Nobel was the inventor of dynamite.', 'science;guest;history;', 2, 1),
(20, 'Alessandro Volta invented the electric battery in the late 1700s.', 'science;guest;history;', 2, 1),
(23, 'Which is a synonym of impecunious?', 'synonyms;vocab;word;', 3, 2);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx` (`question`);

--
-- Index pour la table `mcqchoices`
--
ALTER TABLE `mcqchoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question` (`question`);

--
-- Index pour la table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_index` (`type`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `mcqchoices`
--
ALTER TABLE `mcqchoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`question`) REFERENCES `questions` (`id`);

--
-- Contraintes pour la table `mcqchoices`
--
ALTER TABLE `mcqchoices`
  ADD CONSTRAINT `mcqchoices_ibfk_1` FOREIGN KEY (`question`) REFERENCES `questions` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
